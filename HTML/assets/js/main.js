/* =============================================================================
   jQuery: all the custom stuff!
   ========================================================================== */

"use strict";

$(document).ready(function () {
    /* Subscribe lightbox */
    /*$(".subscribe-me").subscribeBetter({
        trigger: "onload",
        animation: "fade",
        autoClose: true,
        delay: 1500,
        showOnce: true
    });*/
    
    
	/* Video Lightbox */
	if (!!$.prototype.simpleLightboxVideo) {
		$('.video').simpleLightboxVideo();
	}

	/*ScrollUp*/
	if (!!$.prototype.scrollUp) {
		$.scrollUp();
	}

	/*Responsive Navigation*/
	$("#nav-mobile").html($("#nav-main").html());
	$("#nav-trigger span").on("click",function() {
		if ($("nav#nav-mobile ul").hasClass("expanded")) {
			$("nav#nav-mobile ul.expanded").removeClass("expanded").slideUp(250);
			$(this).removeClass("open");
		} else {
			$("nav#nav-mobile ul").addClass("expanded").slideDown(250);
			$(this).addClass("open");
		}
	});

	$("#nav-mobile").html($("#nav-main").html());
	$("#nav-mobile ul a").on("click",function() {
		if ($("nav#nav-mobile ul").hasClass("expanded")) {
			$("nav#nav-mobile ul.expanded").removeClass("expanded").slideUp(250);
			$("#nav-trigger span").removeClass("open");
		}
	});
    

	/* Sticky Navigation */
	if (!!$.prototype.stickyNavbar) {
		$('#header').stickyNavbar({
            activeClass: "active",
            sectionSelector: "scrollto",
            navOffset: 0,
            animDuration: 600,
            startAt: 0, // Stick the menu at XXXpx from the top
            easing: "easeInQuad",
            bottomAnimation: true,
            jqueryEffects: false,
            animateCSS: true,
            animateCSSRepeat: false,
            selector: "a",
            jqueryAnim: "fadeInDown", // jQuery effects: fadeIn, show, slideDown
            mobile: true
        });
	}

	$('#content').waypoint(function (direction) {
		if (direction === 'down') {
			$('#header').addClass('nav-solid fadeInDown');
		}
		else {
			$('#header').removeClass('nav-solid fadeInDown');
		}
	});
    
    
    /**
    ***  Gridder - v1.4.2
    **/
    // REMOVE AND ADD CLICK EVENT 
    $('.doAddItem').on('click', function () {
        $(".gridder").data('gridderExpander').gridderAddItem('TEST');
    });

    // Call Gridder
    $(".gridder").gridderExpander({
        scrollOffset: 120,
        scrollTo: "panel", // "panel" or "listitem"
        animationSpeed: 900,
        animationEasing: "easeInOutExpo",
        onStart: function () {
            console.log("Gridder Inititialized");
        },
        onExpanded: function (object) {
            console.log("Gridder Expanded");
        },
        onChanged: function (object) {
            console.log("Gridder Changed");
        },
        onClosed: function () {
            console.log("Gridder Closed");
        }
    });
    

});


/* Preloader and animations */
$(window).load(function () { // makes sure the whole site is loaded
	//$('#status').fadeOut(); // will first fade out the loading animation
	//$('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
	//$('body').delay(350).css({'overflow-y': 'visible'});

	/* WOW Elements 
	if (typeof WOW == 'function') {
		new WOW().init();
	}*/

	/* Parallax Effects */
	if (!!$.prototype.enllax) {
		$(window).enllax();
	}

});

