<?php get_header(); ?>
	
	
<div id="wrapper" class="woocommerce">

    <?php get_template_part( 'content', 'header_shop' ); ?>


	<main id="content">
		<!-- section -->
		<section id="catas" class="scrollto">

            <div class="row no-padding-bottom clearfix">
                <div class="s-col-1">

                    <h1><?php esc_html_e( 'Page not found', 'html5blank' ); ?></h1>
                    <h2>
                        <a href="<?php echo esc_url( home_url() ); ?>"><?php esc_html_e( 'Return home?', 'html5blank' ); ?></a>
                    </h2>
                    
                </div>
            </div><!-- /.row -->
            
            <div class="row clarfix">
                <div class="s-col-1 col-assembly">  
                     <div class='assembly'><div class='dot-row'><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div></div><div class='dot-row'><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div></div><div class='dot-row'><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div></div><div class='dot-row'><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div></div><div class='dot-row'><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div></div><div class='dot-row'><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div></div><div class='dot-row'><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div></div><div class='dot-row'><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div><div class='dot'></div></div></div>
                </div>
            </div><!-- /.row -->

		</section>
		<!-- /section -->
	</main>


<?php get_sidebar(); ?>

<?php get_footer(); ?>
