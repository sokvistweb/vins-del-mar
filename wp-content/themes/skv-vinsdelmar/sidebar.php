
    <!--Contact-->
    <aside id="contacto" class="scrollto text-center contact">

        <div class="row clearfix">

            <div class="section-heading">
                <h2 class="section-title">Contacto</h2>
            </div>

            <div class="s-col-3 testimonial classic">
                <h4>Cala Jóncols</h4>
                <p><a href="#">Tel.	0034 972 25 39 70</a><br>
                <a href="#">Tel. 0034 972 19 90 28</a><br>
                <a href="#">info@calajoncols.com</a></p>
                <p>Latitud 42º17'22.2''N<br>
                    Longitud 3º16'40.116''E Canal 13 VHF</p>
            </div>

            <div class="s-col-3">
                <a class="google-img" href="https://www.google.com/maps/dir//42.2535909,3.2530475/@42.2538464,3.2508506,17z?hl=ca" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/google-maps.jpg" width="500" height="340" alt="Google maps"/></a>
            </div>

            <div class="s-col-3 testimonial classic">
                <!-- Mailchimp Subscribe Form -->
                <div id="mc_embed_signup" class="subscribe-form">
                    <h4>Reserva tus vinos o suscríbete para recibir información</h4>
                    <form action="https://calajoncols.us12.list-manage.com/subscribe/post?u=c11b55309325dd66939d5f671&amp;id=8a3a503ebd" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">

                        <div class="mc-field-group">
                            <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email">
                        </div>

                        <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Nombre">
                        <div id="mce-responses" class="clear">
                            <div class="response" id="mce-error-response"></div>
                            <div class="response" id="mce-success-response"></div>
                        </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                        <div id="real-people" aria-hidden="true"><input type="text" name="b_c11b55309325dd66939d5f671_8a3a503ebd" tabindex="-1" value=""></div>
                        <div class="clear">
                            <input type="submit" value="Enviar" name="subscribe" id="mc-embedded-subscribe" class="button">
                        </div>
                    </form>
                </div> <!-- /.subscribe-form -->
            </div>
            <!-- End of Testimonial-->

        </div>

    </aside>
    <!--End of Testimonials-->

    <div class="row clearfix blue"></div>

</main>
<!--End Main Content Area-->


<!-- Mailchimp Reserve Bottle Form -->
<div id="mc_embed_signup" class="subscribe-me">
    <p>El número de botellas que hemos sumergido para envejecer bajo el agua es limitado. Resérvate una botella y te la enviamos cuando haya transcurrido su proceso de 6 meses.</p>
    <a href="#close" class="sb-close-btn"><i class="fa fa-times-circle"></i></a>
    <form action="https://calajoncols.us12.list-manage.com/subscribe/post?u=c11b55309325dd66939d5f671&amp;id=8a3a503ebd" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">

        <div class="mc-field-group">
            <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email">
        </div>

        <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Nombre">
        <div id="mce-responses" class="clear">
            <div class="response" id="mce-error-response"></div>
            <div class="response" id="mce-success-response"></div>
        </div>
        <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
        <div id="real-people" aria-hidden="true"><input type="text" name="b_c11b55309325dd66939d5f671_8a3a503ebd" tabindex="-1" value=""></div>
        <div class="clear">
            <input type="submit" value="Haz tu reserva aquí" name="subscribe" id="mc-embedded-subscribe" class="button">
        </div>
    </form>
</div>
<!-- /.subscribe-form -->


<!-- EU popup -->
<div class="eupopup-container eupopup-container-bottomleft"> 
    <div class="eupopup-markup">
        <div class="eupopup-head">Este sitio web usa cookies</div> 
        <div class="eupopup-body">Si continúa navegando, consideramos que acepta su uso.</div> 
        <div class="eupopup-buttons"> 
            <a href="#" class="eupopup-button eupopup-button_1">Acceptar</a> 
            <a href="https://www.calajoncols.com/es/politica-de-cookies/" target="_blank" class="eupopup-button eupopup-button_2">Més info</a>
        </div> 
        <div class="clearfix"></div> 
        <a href="#" class="eupopup-closebutton">
            <i class="fa fa-times-circle"></i>
        </a> 
    </div> 
</div>
