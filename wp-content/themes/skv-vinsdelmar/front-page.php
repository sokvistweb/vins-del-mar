<?php get_header(); ?>
   
   
<div id="wrapper">

    <?php get_template_part( 'content', 'header' ); ?>
   
    
    <!--Wine Content Area-->
    <main id="content">

        <!--Introduction-->
        <section id="vinos" class="introduction scrollto">

            <div class="row row-first row-flex">
                <?php query_posts('post_type=page&name=shop'); while (have_posts ()): the_post(); ?>
                <div class="s-col-4">
                    <div class="section-heading">
                        <h2 class="section-title"><?php the_field('titulo_shop'); ?></h2>
                        <p class="section-intro"><?php the_field('vinos_info'); ?></p>
                    </div>
                </div>
                <div class="s-col-3-4">
                        <p class="section-subtitle"><?php the_content(); ?></p>
                </div>
                <?php endwhile; ?>
            </div>
            
            
            <!-- Gallery -->
            <div class="row row-second clearfix">
                <div class="s-col-1">
                    <ul class="gallery">
                    <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/gallery-images/th_gallery-image-5.jpg" alt="Landing Page" width="415" height="276" /></li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/gallery-images/th_gallery-image-6.jpg" alt="Landing Page" width="415" height="276" /></li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/gallery-images/th_gallery-image-4.jpg" alt="Landing Page" width="415" height="276" /></li>
                    </ul>
                </div>
            </div>
            <!-- End of Gallery -->


            <!-- Shop -->
            <div class="row row-second clearfix">
                <div class="s-col-1">

                    <!-- Shop menu -->
                    <ul class="gridder grid cs-style-3">
                        <?php if (have_posts()) : ?>
                        <?php query_posts(array( 'post_type' => 'product', 'order' => 'ASC', 'posts_per_page' => 99 )); ?>
                        <?php $counter = -1; while (have_posts()) : the_post(); $counter++ ?>
                        <li class="gridder-list" data-griddercontent="#gridder-content-<?php echo $counter; ?>">
                           <h2><?php the_title(); ?></h2>
                           <span class="sold-out-text"><?php the_field('disponibilidad'); ?></span>
                            <figure class="effect-bubba bubba3">
                                <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                                    <?php the_post_thumbnail('shop_single'); ?>
                                <?php endif; ?>
                                <figcaption>
                                    <h3><?php the_field('familia'); ?></h3>
                                    <span><?php the_excerpt(); ?></span>
                                    <a href="#">Comprar</a>
                                </figcaption>			
                            </figure>
                            <!-- No closing </li> to remove space between inline-block elements http://codepen.io/chriscoyier/pen/hmlqF -->
                        <?php endwhile; ?>
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>
                    </ul>
                    <!-- End Shop menu -->

                    <!-- Shop content -->
                    <?php if (have_posts()) : ?>
                    <?php query_posts(array( 'post_type' => 'product', 'order' => 'ASC', 'posts_per_page' => 99 )); ?>
                    <?php $counter = -1; while (have_posts()) : the_post(); $counter++ ?>
                    <div id="gridder-content-<?php echo $counter; ?>" class="gridder-content">
                        <div class="row">
                            <div class="s-col-2">
                                <?php
                                    global $product;

                                    $attachment_ids = $product->get_gallery_attachment_ids();

                                    foreach( $attachment_ids as $attachment_id ) {
                                        echo $image = wp_get_attachment_image( $attachment_id, 'full' );
                                    }
                                ?>
                            </div>
                            <div class="s-col-2">
                                <div class="sk-item-info summary entry-summary">

                                    <h2 class="product_title entry-title"><?php the_title(); ?></h2>
                                    <div class="woocommerce-product-details__short-description">
                                        <p><?php the_excerpt(); ?></p>
                                        <p><?php the_field('medida'); ?></p>
                                    </div>

                                    <!-- Product price -->
                                    <p class="price"><span class="woocommerce-Price-amount amount"><?php echo $product->get_price_html(); ?></span></p>

                                    <!-- Quantity & Add to cart button -->
                                    <form class="cart" method="post" enctype='multipart/form-data'>
                                    <?php
                                        /**
                                         * @since 2.1.0.
                                         */
                                        do_action( 'woocommerce_before_add_to_cart_button' );

                                        /**
                                         * @since 3.0.0.
                                         */
                                        do_action( 'woocommerce_before_add_to_cart_quantity' );

                                        woocommerce_quantity_input( array(
                                            'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
                                            'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
                                            'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( $_POST['quantity'] ) : $product->get_min_purchase_quantity(),
                                        ) );

                                        /**
                                         * @since 3.0.0.
                                         */
                                        do_action( 'woocommerce_after_add_to_cart_quantity' );
                                    ?>

                                    <button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="add-to-cart single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>

                                    <?php
                                        /**
                                         * @since 2.1.0.
                                         */
                                        do_action( 'woocommerce_after_add_to_cart_button' );
                                    ?>
                                    </form>
                                    <!-- /Quantity & Add to cart button -->

                                    <!-- Share buttons -->
                                    <div class="share-icons">
                                    <p>Comparte</p>
                                    <ul class="icons">
                                        <li>
                                            <a href="https://twitter.com/intent/tweet?url=<?php the_permalink(); ?>&text=<?php the_title_attribute(); ?>" title="Comparte en Twitter" class="twitter" target="_blank">
                                                <i class="fa fa-twitter fa-1x"></i>
                                                <span class="label">Twitter</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" title="Comparte en Facebook" class="facebook" target="_blank">
                                                <i class="fa fa-facebook fa-1x"></i>
                                                <span class="label">Facebook</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" title="Comparte en Google+" class="googleplus" target="_blank">
                                                <i class="fa fa-google-plus fa-1x"></i>
                                                <span class="label">Google+</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="whatsapp://send?text=<?php the_permalink(); ?>" data-action="share/whatsapp/share" title="Comparte en WhatsApp" class="whatsapp" target="_blank">
                                                <i class="fa fa-whatsapp fa-1x"></i>
                                                <span class="label">WhatsApp</span>
                                            </a>
                                        </li>
                                    </ul>
                                    </div>

                                </div>
                                <!-- entry-sumary -->

                                <div class="product-content">
                                    <h3 class="product_subtitle entry-subtitle">Nota de cata:</h3>
                                    <p><?php the_content(); ?></p>
                                </div>
                            </div>
                        </div><!-- /.row -->
                    </div>
                    <?php endwhile; ?>
                    <?php endif; ?>
                    <?php wp_reset_query(); ?>
                    <!-- End Shop content -->
                </div><!-- /.s-col-1" -->
            </div><!-- /.row -->
            <!-- End of Shop -->

        </section>

        <!--Content Section-->
        <section id="catas" class="scrollto">

            <div class="row no-padding-bottom clearfix">


                <!--Content Left Side-->
                <div class="s-col-3">
                    <?php the_field('catas_izquierda'); ?>
                </div>
                <!--End Content Left Side-->

                <!--Content of the Center column-->
                <div class="s-col-3">
                    <?php the_field('catas_centro'); ?>
                </div>
                <!--End Content Center column-->

               <!--Content Right Side-->
                <div class="s-col-3">
                    <img class="tasting" src="<?php echo get_template_directory_uri(); ?>/assets/images/cata-vinos-del-mar.jpg" width="590" height="790" alt="Vins del Mar"/>
                </div>
                <!--End Content Right Side-->
            </div>

        </section>
        <!--End of Content Section-->


        <!--Content Section-->
        <section id="estudio" class="scrollto">

            <div class="row no-padding-bottom clearfix">


                <!--Content Left Side-->
                <div class="s-col-3">
                    <?php the_field('estudio_izqluierda'); ?>
                </div>
                <!--End Content Left Side-->

                <!--Content of the Center column-->
                <div class="s-col-3">
                    <?php the_field('estudio_centro'); ?>
                </div>
                <!--End Content Center column-->

                <!--Content of the Right Side-->
                <div class="s-col-3">
                    <img class="tasting" src="<?php echo get_template_directory_uri(); ?>/assets/images/estudio-tecnico.jpg" width="1000" height="665" alt="Vins del Mar"/>

                    <?php the_field('estudio_derecha'); ?>
                </div>
                <!--End Content Right Side-->
            </div>

        </section>
        <!--End of Content Section-->
            

<?php get_sidebar(); ?>

<?php get_footer(); ?>