<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-47544981-20"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-47544981-20');
    </script>
	
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <title><?php wp_title( '' ); ?><?php if ( wp_title( '', false ) ) { echo ' : '; } ?><?php bloginfo( 'name' ); ?></title>
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="description" content="Vinos ecológicos y biodinámicos envejecidos durante 6 meses bajo el mar de la Bahía de Rosas. Venta online y catas maridadas en el Hotel Cala Jóncols." />
    <meta name="keywords" content="" />
    <meta name="author" content="sokvist.com">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon.ico" rel="shortcut icon">
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/apple-touch-icon.png" rel="apple-touch-icon">
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/apple-touch-icon-precomposed.png" rel="apple-touch-icon-precomposed">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/style.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800' rel='stylesheet' type='text/css'>
    
    <link rel="canonical" href="https://vinsdelmar.com">
    <!--<link hreflang="ca" href="https://vinsdelmar.com/ca/" rel="alternate">
    <link hreflang="en" href="https://vinsdelmar.com/en/" rel="alternate">
    <link hreflang="fr" href="https://vinsdelmar.com/fr/" rel="alternate">-->
    
    <!-- Facebook Open Graph -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://vinsdelmar.com/">
    <meta property="og:title" content="Vins del mar">
    <meta property="og:image" content="assets/images/og-image.jpg">
    <meta property="og:description" content="Vinos ecológicos y biodinámicos envejecidos durante 6 meses bajo el mar de la Bahía de Rosas. Venta online y catas maridadas en el Hotel Cala Jóncols.">
    <meta property="og:site_name" content="Vins del mar">
    <meta property="og:locale" content="es">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="630">
    
    <!-- Twitter Card 
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="">
    <meta name="twitter:creator" content="@SokvistWeb">
    <meta name="twitter:url" content="http://vinsdelmar.com/">
    <meta name="twitter:title" content="Vins del mar">
    <meta name="twitter:description" content="Cuando en 2009 decidimos guardar vinos en nuestra propia bodega, no quisimos construir una cava subterránea sino aprovechar las mejores condiciones que nos daba nuestro entorno: el fondo del mar.">
    <meta name="twitter:image" content="assets/images/og-image.jpg">-->
    
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/vendor/modernizr.custom.js"></script>

    <?php wp_head(); ?>

	</head>
	<body <?php body_class(); ?>>
        
        <!-- Preloader -->
        <div id="preloader">
            <div id="status" class="la-ball-triangle-path">
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
        <!--End of Preloader-->

        <div class="eupopup eupopup-top"></div>

        <div class="page-border" data-wow-duration="0.7s" data-wow-delay="0.2s">
            <div class="top-border wow fadeInDown animated"></div>
            <div class="right-border wow fadeInRight animated"></div>
            <div class="bottom-border wow fadeInUp animated"></div>
            <div class="left-border wow fadeInLeft animated"></div>
        </div>

