<?php get_header(); ?>
    

<div id="wrapper" class="woocommerce">

    <?php get_template_part( 'content', 'header_shop' ); ?>


	<main id="content">
		<!-- section -->
		<section class="scrollto">

            <div class="row no-padding-bottom clearfix">
                
                <div class="s-col-1">

                <h1><?php the_title(); ?></h1>

                <?php if ( have_posts()) : while ( have_posts() ) : the_post(); ?>

                    <?php the_content(); ?>

                <?php endwhile; ?>
                <?php endif; ?>
            
                </div>
		
            </div><!-- /.row -->

		</section>
		<!-- /section -->
	</main>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
