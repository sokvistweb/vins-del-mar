        <!--Footer-->
        <footer id="landing-footer" class="clearfix">
            <div class="row clearfix">

                <p id="copyright" class="s-col-2"><a href="https://www.calajoncols.com/es/aviso-legal-y-politica-de-privacidad/" title="Aviso legal" target="_blank">Aviso legal</a> | <a href="https://www.calajoncols.com/es/politica-de-cookies/" title="Política de cookie" target="_blank">Política de cookies</a> | <a href="https://www.calajoncols.com/es/politica-de-privacidad/" title="Política de privacidad" target="_blank">Política de privacidad</a> | Made by <a href="https://sokvist.com">Sokvist</a></p>

                <!--Social Icons in Footer-->
                <ul class="s-col-2 social-icons">
                    <li><a target="_blank" title="Visítanos en Facebook" href="https://www.facebook.com/hotelcalajoncols/"><i class="fa fa-facebook fa-1x"></i><span>Facebook</span></a></li>
                    <li><a target="_blank" title="Visítanos en Google+" href="https://plus.google.com/+Calajoncols"><i class="fa fa-google-plus fa-1x"></i><span>Google+</span></a></li>
                    <li><a target="_blank" title="Visíta nuestro canal de YouTube" href="https://www.youtube.com/channel/UCumfFz75pQFN3ecQSkXu5WA"><i class="fa fa-youtube-square fa-1x"></i><span>YouTube</span></a></li>
                    <li><a target="_blank" title="Visítanos en Instagram" href="https://www.instagram.com/calajoncols/"><i class="fa fa-instagram fa-1x"></i><span>Instagram</span></a></li>
                </ul>
                <!--End of Social Icons in Footer-->
            </div>
        </footer>
        <!--End of Footer-->

		
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/vendor/jquery.1.8.3.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/min/plugins.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/min/main.min.js"></script>

    <?php wp_footer(); ?>

	</body>
</html>
